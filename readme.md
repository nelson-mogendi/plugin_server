# Overview
---
A web app used to manage plugins hosted in a Github account.  
Repositories in Github are known as plugins in the web app.  
And tags in Github are known as versions in the web app.  

## View the documentation
---
The documentation is made using [Mkdocs](https://www.mkdocs.org/) and store in the `docs` folder.  
To view the documentation, run the following commands in the root folder.  
`cd docs`  
`mkdocs serve`  

## References used to create the web app
[Python version handling](https://packaging.pypa.io/en/latest/version.html#packaging.version.parse)  
[Semantic versioning](https://semver.org/)  