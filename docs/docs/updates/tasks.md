# Overview  
---
Holds functions that choose which version a plugin will update to.  


## `def get_latest_version(platform, plugin_version)`  
---
Receives two arguments:
1. The platform in string format.  
2. The version number in string format.  
It then uses these variables to find the best appropriate version for the plugin to upload to.  
Example of arguments passed:  
`def get_latest_version('odoo', '1.2')`  
Example of response:
```
{
    'current_version': '2.1.3',
    'security_version': '1.2.1'
}
```  

## `def get_latest_in_major_version(version)`  
---
Receives a version number(in query format) and returns the latest improvement within that major version or returns the same version.  
Example request:
`def get_latest_in_major_version(<Versions: 1.1.2>)`  
Example response:
`1.5.6`  
Where that response will be given even if there is a version 2.0 that exists somewhere.
