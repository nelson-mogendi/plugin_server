#  Overview
---
These are the forms used in the `account` app.  
The `account` app handles authentication features and user management.  

## `class UserAuthenticationForm(forms.ModelForm):`  
Form is used when logging in users. If the email and password do not match, an error is raised through the `forms.ValidationError`  
It inherits the `ModelForm`, meaning it only recognizes the fields declared in the model specified, which in this in this case is `Users`.  
