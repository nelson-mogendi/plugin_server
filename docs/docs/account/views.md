#  Overview
---
These are the models used in the `account` app.  
The `account` app handles authentication features and user management.  

## `def sign_in(request)`  
---
This function renders the template used to log in users.  
It receives the input data(``email``, ``password``) through the `UserAuthenticationForm` form.  
If the form is valid, the user is logged in through the `login(request, user)` method and redirected to another page.  
If the form is not valid, the user is redirected to the same page where a form error is shown.  
  
  
## `def sign_out(request)`
---
The function logs out a user and redirects to the `def sign_in(request)`.