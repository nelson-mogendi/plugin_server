#  Overview
---
These are the models used in the `account` app.  
The `account` app handles authentication features and user management.  


## ``class UserManager(BaseUserManager)``  
---
Used to customize the Django user model.  
So, the `def create_user()` and `def create_superuser()` are extended to allow a user and superuser to be created using `email` and `password` fields.


## `class Users(AbstractBaseUser)`  
---
This the model that stores user and superusers data.  
The model's unique idenditifier field is the `email` field.  
The `password` field is not defined because it is inherited from the `AbstractBaseUser` class.  
Since there are no additional required fields declared in `REQUIRED_FIELDS = []`, the default required fields are `email` and `password`