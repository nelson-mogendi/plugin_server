#  Overview  
---
Handles HTTP requests and serves back a template.  

## `def plugins(request)`  
---
It is the home page you receive once logged in.  
The response is a template containing repositories and active version.  
Accessible url: `/plugins`  


## `def versions(request, plugin_name)`  
---
Returns the versions related to a plugin to be viewed.  
Accessible url: `/versions/<str:plugin_name>`  
Example url: `/versions/Woocommerce`  


## `class PlatformList(ListView)`  
---
Generic view that list all platforms.  
Accessible url: `/platforms`  

## `class PlatformCreate(CreateView)`  
Generic view used to create a platform.  
Accessible url: `/create-platform`  


## `class PlatformUpdateView(UpdateView)`  
---
Generic view used to update platform details.  
Accessible url: `/update-platform/<pk>`  
Example url: `/update-platform/12`  


## `class PlatformDeleteView(DeleteView)`  
---
Generic view used to delete a platform.  
Accessible url: `/delete-platform/<pk>`  
Example url: `/delete-platform/12`  


## `def versiondetails(request, pk)`  
---
Hosts the view and form used to update information regarding a specified version.  
Accessible url: `/version-details/<pk>`  
Example url: `/version-details/7`  
