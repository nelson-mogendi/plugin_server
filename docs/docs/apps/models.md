#  Overview
---
Its the database that host data about plugins.  


## `class Platform(models.Model)`  
---
It host information about the platform. The fields are:  
1. `name`: What the platform is referred to in text e.g Woocommerce.  
2. `image`: The image of the platfrom in a link format.  


## `class Repository(models.Model)`  
---
The web app retrieves Github repositories and store their information in this model. The fields are:  
1. `name`:  Do not update or modify this field. It stores the name as it appears in the Github account.  
2. `title`: A pseudo name used to refer to repository name. You shouldn't change the `name` field but you can update or modify the `title` field.  
3. `platform`: The foreign key to the platform model. 


##  `class Versions(models.Model)`  
---
Stores information regarding a version number. The fields are:  
1. `repository`: The foreign key to the Repository model.  
2. `version_number`: Do not update or modify this field. It refers to the tags as created in the repository (in the Github account).  
3. `security_version`: Referes to another version number that fixes bugs or flaws in the specified version number.  
4. `active`: Defines whether the version number is the stable version of the a repository.  
5. `author`: The email of the person who created that version number or fixed it.  
6. `download_link`: A link to download that version number in zip format.  
7. `code_link`: A link to view the code of that version number in Github.  
