#  Overview  
---
Functions that execute complex logic that can not be included in other areas due to their length.  
It also reduces chances of circular import errors or import clashes.

## `def get_all_repositories()`  
---
Used to initiate access to the Github account that hosts the repositories.  
The key field is `GITHUB_TOKEN` which is a hashed key stored in the virtual environment for security reasons.  

##  `def update_repository_database()` 
--- 
Loops through each repository in the Github account and updates the database if they are new repositories.  

## `def update_version_database()`  
---
Queries if the repository has any tags and saves those tags as versions in the database.  


## `def plugins_view_data()`  
---
Queries repositories from the database as well as their active versions.
An example of a response after calling this functions is:  
```
{
    'Woocommerce': {active_version: 'v1.2.0'},
    'Odoo': {active_version: 'v1.1.0'},
}
```  


## `def get_platform_choices()`  
---
Queries the available platform names.  
Example response is a list:  
```
    ['Woocommerce', 'Odoo']
```  


## `def version_data(version_pk)`  
---
Receives an int type argument which is a primary key.  
It then returns primary information about that specific version.
Example response is:  
```
{
    'platform_image': 'https://woocommerce.com/woocommerce.png',
    'plugin_name': 'iPay Cart',
    'version_number': 'v1.1.0',
    'download_link': 'https://ipay.github.com/zip',
    'code_link': 'https://ipay.github.com/ipaycart',
    'platform_name': 'Woocommerce',
    'author_email': 'johndoe@gmail.com',
    'active': True,
    'notes': 'Plugin for shopping in Wordpress'
    'security_version': 'v1.5.1',
    'related_versions': ['v1.6.1', 'v1.5.1', 'v1.5.3']
}
```  

## `def change_version_state(version_pk, activate)`  
---
The function responsible for changing the stable version state.  
Only one version can be active at a given time and it will be treated as the public release.
The argument are `version_pk` which is an integer and `activate` which a boolean value in string format.
Example arguments:  
`change_version_state(2, 'True')`
*Note: Only one version is active at a given time, but all versions can be inactive.*


## `def activate_version(version_pk)`  
---
Activates a version number.


## `deactivate_version(version_pk)`  
---
Deactivates a version number.  


## `def get_versions_related_to_repository(repository)`  
---
Query any versions related to a specified repository and return that result in a list format.  
Example response:  
```['v1.6.1', 'v1.5.1', 'v1.5.3']```  
