#  Overview
---
These are the forms used in the `apps` app.  
The `apps` handles the management and viewing of plugins.

## `class PluginVersionForm(forms.Form):`  
Form is used to update versions for a certain repository/plugin.  
The fields are:  
1. `plugin_name`: Use this field to set a different name from that of the repository. For instance, if a repository name is `odoo-plugin`, you can set the plugin name to be `Odoo`.  
The `plugin_name` is treated as the pseudo name of a repository.  
2. `email`: The email of the creator who created that specific version of the plugin or fixed a bug in that version. Version 1.1 can have a different creator from version 1.2.  
3. `platform`: Refers to the platform that the plugin is built for. Platforms can be Woocommerce, Odoo.  
4. `activate`: Sets a version as the stable version. A stable version does not have to be the latest version.  
5. `security_version`: Refers to the version that solves a specific bug in that version.  
6. `notes`: The area to provide more information about the plugin.  