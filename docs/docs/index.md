# Welcome to Plugin Server documentation

This a web app built to manage plugins hosted on one git repository using Django, Python3, Sqlite.  
The web app serves the following functions:  
1. Receive post request from plugins and return data stating the latest plugin version  
2. Have an interface that allows easy management of the plugins  



## How To Setup Project On A Local Computer
---
1. Install ``python 3.8+`` on your computer.
2. Create a folder e.g `mkdir <folder name>`
3. Navigate into that folder `cd <folder name>`
4. Initialize git `git init`
5. Clone the project `git clone <link>`
6. Create a virtual environment to install packages `python3 -m venv venv`
7. Activate the virtual environment `source venv/bin/activate`
8. Packages are listed inside the `requirements` folder and you need to install these packages into your virtual environment `pip install -r requirements/requirements`
9. Once done check packages that have been installed using `pip list` command
10. Run the `ls` command to see if you have all the files.
11. You may or may not have the `env` folder, so go ahead and create it. The `env.sh` file is used to store secret keys and other configurations that should not be pushed online.
12. You can also add a `db.sqlite` file in case you don't have it.
13. The `docs` folder stores the project documentation. To view the documentation, navigate into that folder `cd docs`
14. Then run `mkdocs server`. Open the port `http://localhost:8000` and you will see the documentation.
15. Use `CTRL+C` to stop the mk_docs server and go back to the root folder using `cd ..`
16. Your project folder layout should now look like this:  
```
folder_name  # Root folder
|-- .git
|-- account
|-- core
|-- docs
|-- env
    |-- env.sh
|-- requirements
|-- templates
|-- venv
| setup
| manage.py
| .gitignore
| db.sqlite3
```  
  
  
## The `env/env.sh` file
---
The `env.sh` file stores the secret variables that are loaded in the virtual environment and later used in the `core/settings.py`.  
In case you are creating your own `env.sh` file, here are the variables that you need in the `env.sh` file:  

* ``SECRET_KEY`` 
* ``DEBUG``  
* ``ALLOWED_HOSTS``  
* ``GITHUB_TOKEN``  
* ``GITHUB_USERNAME``   
  
An example of how the above variables are configured in the `env.sh` file is:  
```
#!/bin/bash  

export DEBUG=1  
export ALLOWED_HOSTS='*'  
```


## How to start the server
---
If you have the `env/env.sh` file, run `source env/env.sh` to activate the virtual environment.  
If you don't have it, create an `env/env.sh` file and directly write the secrets needed in the `core/settings.py` into that file.  
Then activate the virtual environment by running `source venv/bin/activate`.  
After that, you load the keys into the virtual environment using `source env/env.sh`
Finally, run `python manage.py runserver` to start the server.  
  
## Commands
### Creating a user
---
The web app necessitates a user to be created before they can log in.  
There are no password reset or create account pages on the web app because this is not a publicy open project.  
To create a normal user follow the commands below:  
1. Activate the virtual environment by running `source venv/bin/activate` in the root folder.  
2. Load the secret variables by running  
`source env/env.sh`  
3. To enter the django shell, run  
`python manage.py shell`  
4. Then while inside the django shell, load the models, specifically those in `account/models.py` because those are related to users:  
`from accounts.models import Users`  
5. Create a new user by running:  
`user = User.objects.create_user(email='<user_email>', password='<user_password>)`  
6. Exit the django shell:  
`exit()`  


## How to update repository and versions
---
To update repository and versions, follow the procedure below:  
1. Activate the virtual environment by running `source venv/bin/activate` in the root folder.  
2. Load the secret variables by running  
`source env/env.sh`  
3. Start the server by running:  
`python manage.py runserver`  
4. Login to the admin panel through the link: `/admin`   
5. After that you'll be redirected to the Site Administration panel.  
6. Click on Repositories and select one or two rows.  
7. And in the action bar select `update_repositories_and_versions` and click Go.  
8. That updates the repositories and versions.  


## How to add a platform  
---
1. Login in to the web app as a normal user.  
2. In the navigation bar, click `+ Platform` and you'll be redirected to the `/platforms` link.  
3. Find the `Add Platform` button and click on it.  
4. A form will be provided with a `Name` field and `Platform image url`. Add a name in text format and a link in url format. The form does not upload images.  
5. Once done, click on the `Create platform` button to add the platform.  
6. If there are no errors, you'll be redirected to the `/platforms` link.  


## Serving updates  
---
Post data to the `/update-plugin` in JSON format. Example of the data expected is:  
```
{
    'name': 'Odoo',  # The platform that the plugin is built for
    'version': '14',  # The platfrom version
    'plugin_version': '1.1.4'  # The plugin version
}
```  
The web app will find the appropriate (or latest version) for that platform and return that information in JSON format.  
Example of this response is:
```
{
    'current_version': '2.1.3',  # The latest plugin released
    'security_version': '1.2.1'  # But first update to this version
}
```  