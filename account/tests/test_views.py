from django.test import TestCase
from account.models import Users
from django.urls import reverse


class LoginViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Users.objects.create_user(
            email='johndoe@gmail.com', password='notsecure'
        )

    def test_login_view_url_accessible_by_name(self):
        response = self.client.get(reverse('signin'))
        self.assertEqual(response.status_code, 200)

    def test_login_view_uses_correct_template(self):
        response = self.client.get(reverse('signin'))
        self.assertTemplateUsed(response, 'account/login.html')

    def test_login_view_redirects_on_valid_form_submittion(self):
        response = self.client.post(
            reverse('signin'),
            {'email': 'johndoe@gmail.com', 'password': 'notsecure'}
        )
        self.assertRedirects(response, reverse('plugins'))


class LogoutViewTest(TestCase):
    def test_logout_view_url_exists_at_desired_location(self):
        response = self.client.get(reverse('signout'))
        self.assertRedirects(response, '/login')
