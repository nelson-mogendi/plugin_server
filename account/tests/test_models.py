from django.test import TestCase
from account.models import Users


class UsersModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Users.objects.create_user(
            email="johndoe@gmail.com", password="notsecure"
        )

        Users.objects.create_superuser(
            email="mitchelle@gmail.com", password="notsecure"
        )

    def test_string_representation(self):
        user = Users.objects.get(email="johndoe@gmail.com")
        self.assertEqual(str(user), "johndoe@gmail.com")

    def test_user_is_active(self):
        super_user = Users.objects.get(email="mitchelle@gmail.com")
        user = Users.objects.get(email="mitchelle@gmail.com")

        self.assertEquals(True, user.is_active)
        self.assertEquals(True, super_user.is_active)

    def test_user_is_staff(self):
        super_user = Users.objects.get(email="mitchelle@gmail.com")
        user = Users.objects.get(email="johndoe@gmail.com")

        self.assertEquals(False, user.is_staff)
        self.assertEquals(True, super_user.is_staff)

    def test_user_is_admin(self):
        super_user = Users.objects.get(email="mitchelle@gmail.com")
        user = Users.objects.get(email="johndoe@gmail.com")

        self.assertEquals(False, user.is_admin)
        self.assertEquals(True, super_user.is_admin)

    def test_get_full_name_decorator(self):
        user = Users.objects.get(email="mitchelle@gmail.com")
        self.assertEquals(user.get_full_name(), "mitchelle@gmail.com")

    def test_user_has_perm(self):
        super_user = Users.objects.get(email="mitchelle@gmail.com")
        user = Users.objects.get(email="johndoe@gmail.com")

        self.assertTrue(super_user.has_perm(super_user.is_admin, super_user))
        self.assertFalse(user.has_perm(user.is_admin, user))

    def test_has_module_perms(self):
        super_user = Users.objects.get(email="mitchelle@gmail.com")
        user = Users.objects.get(email="johndoe@gmail.com")

        self.assertTrue(super_user.has_module_perms('authentication'))
        self.assertFalse(user.has_module_perms('authentication'))

    def test_user_must_have_email(self):
        with self.assertRaises(ValueError):
            Users.objects.create_superuser(
                email="", password="notsecure"
            )
