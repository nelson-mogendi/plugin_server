from django.test import TestCase
from account.forms import UserAuthenticationForm
from account.models import Users


class UserAuthenticationFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Users.objects.create_user(
            email='johndoe@gmail.com', password='simple'
        )

    def test_form_is_valid_when_all_correct_credentials_are_submitted(self):
        """Test form is valid when correct credentials are submitted."""
        form_data = {
            'email': 'johndoe@gmail.com', 'password': 'simple'
        }
        form = UserAuthenticationForm(data=form_data)

        self.assertTrue(form.is_valid())

    def test_form_is_not_valid_when_one_credential_is_missing(self):
        """Test form is NOT valid when one credentials is missing."""
        form_data = {
            'email': 'johndoe@gmail.com', 'password': ''
        }
        form = UserAuthenticationForm(data=form_data)

        self.assertFalse(form.is_valid())

    def test_form_is_not_valid_when_one_or_all_credentials_are_wrong(self):
        form_data = {
            'email': 'johndoe@gmail.com', 'password': 'wrongpassword'
        }
        form = UserAuthenticationForm(data=form_data)

        self.assertFalse(form.is_valid())
