from django.conf.urls import url
from account import views

urlpatterns = [
    url(r'^$', views.sign_in, name='signin'),
    url(r'^login', views.sign_in, name='signin'),
    url(r'^logout', views.sign_out, name='signout'),
]
