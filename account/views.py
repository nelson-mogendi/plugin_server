from django.shortcuts import render
from django.http import HttpRequest
from account.forms import UserAuthenticationForm
from django.shortcuts import redirect
from django.contrib.auth import (login, logout)
from django.contrib.auth import authenticate
from account.sub_logger import logger


def sign_in(request):
    """View that handles user authentication."""
    assert isinstance(request, HttpRequest)

    login_form = UserAuthenticationForm(request.POST or None)

    if login_form.is_valid():
        logger.info("views.sign_in: Login in form is valid")

        email = login_form.cleaned_data.get('email')
        password = login_form.cleaned_data.get('password')

        user = authenticate(email=email, password=password)
        login(request, user)

        logger.info("views.sign_in: Redirecting to next page")
        return redirect('/plugins')

    return render(
        request, 'account/login.html',
        {'title': 'Login', 'form': login_form}
    )


def sign_out(request):
    """View logs out a user then redirect the page."""
    assert isinstance(request, HttpRequest)
    # Do not name views with keywords 'login' or 'logout' because this are system reserved.
    logout(request)
    logger.info("views.sign_out: Logged user out")

    return redirect(sign_in)
