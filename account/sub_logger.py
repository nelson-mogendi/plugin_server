# Initiate logging

import logging
import core.logger # noqa
# This retrieves a Python logging instance (or creates it)
logger = logging.getLogger(__name__)
