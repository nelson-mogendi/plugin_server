from django.contrib import admin
from account.models import Users
from django.contrib.auth.models import Group


class UsersPage(admin.ModelAdmin):
    list_display = ('date_registered', 'last_login', 'email')
    
    search_fields = ('email',)
    ordering = ('date_registered',)


admin.site.register(Users, UsersPage)
admin.site.unregister(Group)
