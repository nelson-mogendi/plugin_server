from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


# Create your models here.
class UserManager(BaseUserManager):
    """**The Manager for User Model Class.**"""
    def create_user(self, email, password=None, is_active=True):
        """Create a normal user."""
        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password, is_active=True):
        """Create and save a superuser using email and password"""
        user = self.create_user(
            email=self.normalize_email(email),
            password=password
        )

        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class Users(AbstractBaseUser):
    """
        **User Manager Model**

        Custom User class (also known as user class) inheriting AbstractBaseUser class
    """
    email = models.EmailField(verbose_name='email', max_length=60, unique=True)
    date_registered = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []  # Additional required fields apart from email and password are added here.

    objects = UserManager()

    class Meta:
        verbose_name = 'User'

    def __str__(self):
        return self.email

    def get_full_name(self):
        """User is identified by their email"""
        return self.email

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin
