from django import forms
from account.models import Users
from django.contrib.auth import authenticate
from account.sub_logger import logger


class UserAuthenticationForm(forms.ModelForm):
    """Form to log in users """
    class Meta:
        model = Users
        fields = ['email', 'password']

    def clean(self, *args, **kwargs):
        """Custom method to authenticate users on form submit"""

        if self.is_valid():
            email = self.cleaned_data.get('email')
            password = self.cleaned_data.get('password')

            """Check if email and password is correct."""
            if not authenticate(email=email, password=password):
                logger.error('forms.UserAuthenticationForm: Invalid credentials.')
                raise forms.ValidationError('Email or password is incorrect!')
