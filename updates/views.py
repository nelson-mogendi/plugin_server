from django.views import View
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from updates.tasks import get_latest_version


@method_decorator(csrf_exempt, name='dispatch')
class UpdatePlugin(View):
    """Class view only receives POST method and is used to update plugins.
    Example JSON request:
        {
            'name': 'Odoo',  # The platform that the plugin is built for
            'version': '14',  # The platfrom version
            'plugin_version': '1.1.4'  # The plugin version
        }
    Example response:
        {
            'current_version': '2.1.3',  # The latest plugin released
            'security_version': '1.2.1'  # But first update to this version
        }
    """

    def post(self, request):
        """Handle the POST request"""
        received_json_data = json.loads(request.body)

        response_data = get_latest_version(
            received_json_data['name'],  # Pseudo name is Platform/Title
            received_json_data['plugin_version']
        )

        return JsonResponse(response_data)
