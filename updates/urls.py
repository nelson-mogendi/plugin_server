from django.urls import path
from updates.views import UpdatePlugin


urlpatterns = [
    path('update-plugin', UpdatePlugin.as_view(), name='update-plugin'),
]
