from apps.models import (Repository, Versions)
from packaging.version import parse
from django.db.models import Q
"""
You can learn more about the package used to standardize version here:
https://packaging.pypa.io/en/latest/version.html#packaging.version.parse
"""


def get_latest_version(platform, plugin_version):
    """
    Function returns a response with details that inform a plugin
    of the latest version information.
    Example of arguments passed:
        def get_latest_version('odoo', '1.2'):
            ...
    Example of response returned:
        {
            'current_version': '2.1.3',  # The latest plugin released
            'security_version': '1.2.1'  # But first update to this version
        }
    """
    response = {  # The default response data that will be returned
        'current_version': plugin_version,
        'security_version': ''
    }

    """Search if the Platform(i.e Repository) and
    Plugin version(i.e version number) exist in title or name"""
    repository = Repository.objects.filter(
        Q(title=platform) | Q(name=platform)
    )
    version_number = Versions.objects.filter(
        version_number=plugin_version
    )

    """Query if there is an active version for that plugin"""
    active_version = Versions.objects.filter(active=True)

    """If Platform and Plugin version exist, search for the security version."""
    if repository.exists() and version_number.exists():
        security_version = version_number.first().security_version

        """If security version exists, set it in the response"""
        if (security_version is not None) and (security_version != plugin_version):
            response['security_version'] = security_version

        else:
            """Query the latest release within that MAJOR version"""
            response['current_version'] = get_latest_in_major_version(
                version_number.first()
            )

    elif repository.exists() and active_version.exists():
        """
        If no security version exists but there is a repository,
        query if there is an active version for that repository.
        An active version is that which has been approved for release. It does not have to be
        the latest release or earliest release."""
        response['current_version'] = active_version.first().version_number

    return response


def get_latest_in_major_version(version):
    """
    Function returns the latest version with a major version.
    Pass a query object:
        <Version: 1.2.3>
    And the function will return the latest version within that MAJOR version
    (MAJOR.MINOR.PATCH) even if there are other major versions. Example response:
        1.9.9
    """
    specified_version = parse(version.version_number)  # Standardise version scheme
    major_version = specified_version.major  # Get the major version

    """Query versions related to a repository"""
    version_numbers = Versions.objects.filter(
        repository=version.repository
    ).values_list('version_number', flat=True)

    """"Set the specified version as the default latest version since the need is to get
    a version higher than it"""
    latest_in_major_version = specified_version

    """Loop through version numbers for that repository."""
    for version_number in version_numbers:
        standardised_version_number = parse(version_number)  # Standardise version scheme

        """Check whether the version is in the same bracket of the major version"""
        if standardised_version_number.major == major_version:

            """Update latest version if it is greater than the set version"""
            if standardised_version_number > latest_in_major_version:
                latest_in_major_version = standardised_version_number

    return str(latest_in_major_version)
