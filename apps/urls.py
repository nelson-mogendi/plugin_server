from django.conf.urls import url
from django.urls import path
from apps import views
from apps.views import (
    PlatformList, PlatformCreate,
    PlatformUpdateView, PlatformDeleteView
)


urlpatterns = [
    url(r'^plugins', views.plugins, name='plugins'),
    path('versions/<str:plugin_name>', views.versions, name='versions'),
    path('platforms', PlatformList.as_view(), name='platforms'),
    path('create-platform', PlatformCreate.as_view(), name='create-platform'),
    path('update-platform/<pk>', PlatformUpdateView.as_view(), name='update-platform'),
    path('delete-platform/<pk>', PlatformDeleteView.as_view(), name='delete-platform'),
    path('version-details/<pk>', views.versiondetails, name='version-details'),
]
