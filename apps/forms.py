from django import forms


class PluginVersionForm(forms.Form):
    """Used to update information of a plugin version"""
    plugin_name = forms.CharField(
        widget=forms.TextInput(attrs={'name': 'plugin_name'}), required=False
    )
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'name': 'email'}), required=False
    )
    platform = forms.CharField(
        widget=forms.RadioSelect(attrs={'name': 'platform'}), required=False
    )
    activate = forms.CharField(
        widget=forms.RadioSelect(attrs={'name': 'activate'}), required=False
    )
    security_version = forms.CharField(
        widget=forms.RadioSelect(attrs={'name': 'security_version'}), required=False
    )
    notes = forms.CharField(
        widget=forms.Textarea(attrs={'name': 'notes'}), required=False
    )
