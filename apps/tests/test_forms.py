from django.test import TestCase
from apps.forms import (PluginVersionForm, CreatePlatformForm)
from apps.models import Platform


class PluginVersionFormTest(TestCase):
    def test_form_is_valid_when_correct_details_are_submitted(self):
        form_data = {
            'plugin_name': 'Woocommerce', 'platform': 'Wordpress',
            'author': 'johndoe@gmail.com', 'activate': 'True',
            'notes': 'Resolves bug 1.03'
        }
        form = PluginVersionForm(data=form_data)

        self.assertTrue(form.is_valid())

    def test_form_is_valid_if_some_details_are_missing(self):
        form_data = {
            'plugin_name': 'Woocommerce', 'platform': 'Wordpress',
            'author': 'johndoe@gmail.com', 'activate': 'True'
        }
        form = PluginVersionForm(data=form_data)

        self.assertTrue(form.is_valid())


class CreatePlatformFormTest(TestCase):
    @classmethod
    def setUpTestData(self):
        Platform.objects.bulk_create([
            Platform(name="Woocommerce", image="http:woocommerceimage.ml"),
            Platform(name="Shopify", image="http:shopify.ml/")
        ])

    def test_form_is_valid_when_all_details_are_submitted(self):
        form_data = {
            'name': 'Odoo', 'image': 'https://filestorage.com/odoo.png'
        }
        form = CreatePlatformForm(data=form_data)

        self.assertTrue(form.is_valid())

    def test_form_is_not_valid_when_a_few_details_are_submitted(self):
        form_data = {
            'name': 'Odoo', 'image': ''
        }
        form = CreatePlatformForm(data=form_data)

        self.assertFalse(form.is_valid())

    def test_form_is_not_valid_when_simila_name_is_submitted(self):
        form_data = {
            'name': 'Woocommerce', 'image': 'http:woocommerceimage.ml'
        }
        form = CreatePlatformForm(data=form_data)

        self.assertFalse(form.is_valid())
