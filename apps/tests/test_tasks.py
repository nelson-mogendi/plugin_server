from django.test import TestCase
from apps.tasks import (
    plugins_view_data, get_platform_choices, version_data
)
from apps.models import (Repository, Platform, Versions)


class PluginsViewDataTest(TestCase):
    @classmethod
    def setUpTestData(self):
        platform = Platform.objects.create(
            name="Woocommerce", image="http:woocommerceimage.ml"
        )

        repo_one = Repository.objects.create(name='repo_one', platform=platform)
        repo_two = Repository.objects.create(name='repo_two', platform=platform)  # noqa

        Versions.objects.create(repository=repo_one, version_number='v1.1.0', active=True)

    def test_plugins_view_data_returns_correct_response(self):
        Platform.objects.all().delete()  # Delete all platform
        response = plugins_view_data()

        expected_response = {
            'repo_one': {'active_version': 'v1.1.0', 'platform_image': None},
            'repo_two': {'active_version': None, 'platform_image': None}
        }

        self.assertEqual(response, expected_response)

    def test_plugins_view_data_returns_correct_response_when_platform_exists(self):
        response = plugins_view_data()

        self.assertIsNotNone(response['repo_one']['platform_image'])


class GetPlatformChoicesTest(TestCase):
    @classmethod
    def setUpTestData(self):
        Platform.objects.bulk_create([
            Platform(name="Woocommerce", image="http:woocommerceimage.ml"),
            Platform(name="Odoo", image="http:odooimage.ml"),
            Platform(name="AdobeCart", image="http:adobecartimage.ml")
        ])

    def test_task_returns_three_platform_choices(self):
        response = get_platform_choices()
        expected_response = ['AdobeCart', 'Odoo', 'Woocommerce']

        self.assertEquals(len(response), len(expected_response))


class VersionDataTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.platform = Platform.objects.create(
            name="Woocommerce", image="http:woocommerceimage.ml"
        )
        self.repo_one = Repository.objects.create(
            name='repo_one', platform=self.platform
        )
        self.version = Versions.objects.create(
            repository=self.repo_one, version_number='v1.1.0', active=True
        )

    def test_returns_primary_data_when_platform_exists(self):
        response = version_data(self.version.pk)
        expected_response = {
            'repository_name': self.repo_one.name,
            'plugin_name': self.repo_one.title,
            'version_number': self.version.version_number,
            'download_link': None,
            'code_link': None,
            'author_email': None,
            'active': self.version.active,
            'notes': None,
            'security_version': None,
            'related_versions': [self.version.version_number],
            'platform_image': self.platform.image,
            'platform_name': self.platform.name
        }

        self.assertEquals(response, expected_response)

    def test_returns_primary_data_when_platform_does_not_exist(self):
        self.platform.delete()

        response = version_data(self.version.pk)
        expected_response = {
            'repository_name': self.repo_one.name,
            'plugin_name': self.repo_one.title,
            'version_number': self.version.version_number,
            'download_link': None,
            'code_link': None,
            'author_email': None,
            'active': self.version.active,
            'notes': None,
            'security_version': None,
            'related_versions': [self.version.version_number],
            'platform_image': None,
            'platform_name': None
        }

        self.assertEquals(response, expected_response)
