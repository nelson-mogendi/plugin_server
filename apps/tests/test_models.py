from django.test import TestCase
from apps.models import (Platform, Repository)


class PlatformModelTest(TestCase):
    @classmethod
    def setUpTestData(self):
        Platform.objects.bulk_create([
            Platform(name="Woocommerce", image="http:woocommerceimage.ml"),
            Platform(name="Odoo", image="http:odooimage.ml/")
        ])

    def test_string_representation(self):
        platform = Platform.objects.get(name="Woocommerce")
        self.assertEqual(str(platform), "Woocommerce")


class RepositoryModelTest(TestCase):
    @classmethod
    def setUpTestData(self):
        platform = Platform.objects.create(
            name="Woocommerce", image="http:woocommerceimage.ml"
        )
        Repository.objects.create(
            name="new_repo", platform=platform
        )

    def test_string_representation(self):
        repository = Repository.objects.get(name="new_repo")
        self.assertEqual(str(repository), "new_repo")
