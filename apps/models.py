from django.db import models


class Platform(models.Model):
    name = models.CharField(max_length=50, unique=True, null=False)
    image = models.URLField(null=True)

    class Meta:
        verbose_name = 'Platform'

    def __str__(self):
        return self.name


class Repository(models.Model):
    name = models.CharField(max_length=50, unique=True, null=False)
    title = models.CharField(
        help_text='Plugin name. It can be different from the repository name',
        max_length=50, unique=True, null=True
    )
    platform = models.ForeignKey(
        Platform, models.SET_NULL, blank=True, null=True,
    )

    class Meta:
        verbose_name = 'Repository'
        verbose_name_plural = 'Repositories'

    def __str__(self):
        return self.name


class Versions(models.Model):
    repository = models.ForeignKey(
        Repository, on_delete=models.CASCADE, blank=True, null=True,
    )
    version_number = models.CharField(
        verbose_name='The version number or tag', max_length=100, null=True
    )
    security_version = models.CharField(
        verbose_name='The security version that improves/fix bugs in this version',
        max_length=100, null=True
    )
    active = models.BooleanField(default=False)
    author = models.EmailField(
        verbose_name='Author email', null=True
    )
    notes = models.TextField(null=True)
    download_link = models.URLField(null=True)
    code_link = models.URLField(null=True)

    class Meta:
        verbose_name = 'Versions'
        verbose_name_plural = 'Versions'

    def __str__(self):
        return self.version_number
