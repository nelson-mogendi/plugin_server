import github3
from apps.models import (Platform, Repository, Versions)
import os
from apps.sub_logger import logger


def get_all_repositories():
    """Get all repositories of user."""
    # Activate github connection
    github = github3.login(
        token=os.environ['GITHUB_TOKEN']
    )

    return github.repositories()


def update_repository_database():
    """Adds new repositories to database."""
    all_repositories = get_all_repositories()
    repository_names = []

    for repo in all_repositories:
        repository_names.append(repo.name)

    """Query all names in the Repository model"""
    existing_repo_names_query = Repository.objects.values_list('name', flat=True)

    """Difference between two lists gives new repository names"""
    new_repo_names = list(set(repository_names) - set(existing_repo_names_query))

    """Bulk create new repositories in the database."""
    if new_repo_names:
        repository_instance_list = [
            Repository(
                name=repo_name
            ) for repo_name in new_repo_names
        ]
        Repository.objects.bulk_create(repository_instance_list)


def update_version_database():
    """Add repository version(also known as tags in github) to database
    *** Always call this function after calling 'update_repository_database'"""
    all_repositories = get_all_repositories()

    """Loop through github repositories"""
    for repo in all_repositories:
        """Query the repository name in the database"""
        repository = Repository.objects.get(name=repo.name)

        """Loop through tags in specific repository."""
        for tag in repo.tags():
            """Recreate github code link.
            Example:
                https://github.com/git_username/repo_name/tree/b0afa94d4dfc6b8db85840d7b9ff029f8f88d11e
            """
            github_code_link = 'https://github.com/{}/{}/tree/{}'.format(
                os.environ['GITHUB_USERNAME'], repo.name, tag.commit.sha
            )

            Versions.objects.update_or_create(
                download_link=tag.zipball_url,  # The link to download a zip file of the version
                code_link=github_code_link,  # Link to view the code on github
                defaults={
                    'repository': repository,
                    'version_number': tag.name
                }
            )


def plugins_view_data():
    """
    Consolidate data that will be shown on the plugin UI page.
    Currently the data variables are:
        - repository_name  # Also known as plugin
        - active_version  # The active version for the plugin

    Example response is:
        {
            'repo_name': {active_version: 'v1.2.0'},
            'odoo': {active_version: 'v1.1.0'},
            ....,
        }
    """

    response = {}
    active_version = None
    repositories = Repository.objects.all().order_by('name')

    """Loop through repositories in database"""
    for repo in repositories:
        """Query for the active version of the repository.
        Use 'filter' attribute so that if the query returns an empty value, no error is raised."""
        active_version = Versions.objects.filter(
            repository=repo, active=True
        ).values('version_number')

        if active_version.exists():
            active_version = active_version.first()  # Get the first active version
            active_version = active_version['version_number']
        else:
            active_version = None

        """Query the platform image or return None"""
        platform_image = None
        if repo.platform is not None:
            platform_image = repo.platform.image

        response[repo.name] = {
            'active_version': active_version,
            'platform_image': platform_image
        }

    return response


def get_platform_choices():
    """
    Query platform names and return those names in a list format.
    Example response is:
        [
            'Woocommerce', 'Odoo'
        ]
    """
    query_platform_names = Platform.objects.all().values_list('name', flat=True)
    platform_names = list(query_platform_names)

    return platform_names


def version_data(version_pk):
    """
    Display the data related to a version, which may or may not be
    updated.
    Example response is:
        {
            'platform_image': 'https://woocommerce.com/woocommerce.png',
            'plugin_name': 'iPay Cart',  # Or 'plugin_name': None
            'version_number': 'v1.1.0',
            'download_link': 'https://ipay.github.com/zip',
            'code_link': 'https://ipay.github.com/ipaycart',
            'platform_name': 'Woocommerce',
            'author_email': 'johndoe@gmail.com',
            'active': True,
            'notes': 'Plugin for shopping in Wordpress'
            'security_version': 'v1.5.1',
            'related_versions': ['v1.6.1', 'v1.5.1', 'v1.5.3']
        }

    """

    version = Versions.objects.get(pk=version_pk)
    repository = version.repository

    logger.info('tasks.version_data: Query related versions')
    """Get other version related to the specified repository."""
    related_versions = get_versions_related_to_repository(repository)

    response = {}

    response = {
        'repository_name': repository.name,
        'plugin_name': repository.title,
        'version_number': version.version_number,
        'download_link': version.download_link,
        'code_link': version.code_link,
        'author_email': version.author,
        'active': version.active,
        'notes': version.notes,
        'security_version': version.security_version,
        'related_versions': related_versions
    }

    if repository.platform is None:
        logger.info('tasks.version_data: No platform found')

        response['platform_image'] = None
        response['platform_name'] = None
    else:
        logger.info('tasks.version_data: Platform found')

        response['platform_image'] = repository.platform.image
        response['platform_name'] = repository.platform.name

    return response


def change_version_state(version_pk, activate):
    """
    Change version states by calling two different functions.
    """
    logger.info('tasks.change_version_state: Changing version state')
    if activate == "True":
        activate_version(version_pk)

    if activate == "False":
        deactivate_version(version_pk)


def activate_version(version_pk):
    """Shut down other versions and activate selected version"""
    version = Versions.objects.get(pk=version_pk)
    repository = version.repository

    # Shut down all versions
    Versions.objects.filter(repository=repository).update(active=False)

    # Activate specific version only.
    version.active = True
    version.save()

    logger.info('tasks.activate_version: Activated specified version')


def deactivate_version(version_pk):
    """Shut down selected version"""
    version = Versions.objects.get(pk=version_pk)

    version.active = False
    version.save()

    logger.info('tasks.deactivate_version: Deactivated specified version')


def get_versions_related_to_repository(repository):
    """
    Get version related to the repository specified.
    Example response:
        ['v1.6.1', 'v1.5.1', 'v1.5.3']
    """
    logger.info('tasks.get_versions_related_to_repository: Querying versions')
    versions = Versions.objects.filter(repository=repository).values_list(
        'version_number', flat=True
    )
    related_versions = list(versions)

    return related_versions
