from django.shortcuts import render
from django.http import HttpRequest
from apps.models import (Platform, Versions)
from apps.tasks import (
    plugins_view_data, version_data, get_platform_choices,
    change_version_state
)
from apps.sub_logger import logger
from django.views.generic.list import ListView
from django.views.generic.edit import (CreateView, UpdateView, DeleteView)
from apps.forms import PluginVersionForm
from django.urls import reverse_lazy


# Create your views here.
def plugins(request):
    """View the plugins in the Github repository."""
    assert isinstance(request, HttpRequest)
    logger.info('views.plugins: Retrieve plugins data')

    # Retrieve data that will shown on the page
    plugins_info = plugins_view_data()

    return render(
        request, 'apps/plugins.html',
        {'title': 'Plugins', 'plugins_info': plugins_info}
    )


def versions(request, plugin_name):
    """List plugin versions."""
    assert isinstance(request, HttpRequest)
    logger.info('views.plugins: Query plugin versions')

    query_plugin_versions = Versions.objects.filter(repository__name=plugin_name)

    return render(
        request, 'apps/pluginversions.html',
        {
            'title': 'Versions', 'versions': query_plugin_versions, 'plugin_name': plugin_name
        }
    )


class PlatformList(ListView):
    """List the platforms available"""

    model = Platform
    template_name = 'apps/platforms.html'


class PlatformCreate(CreateView):
    """Create new platforms"""

    model = Platform
    fields = '__all__'
    template_name = 'apps/createplatform.html'
    # Use reverse_lazy instead of reverse to prevent circular import error
    success_url = reverse_lazy('platforms')


class PlatformUpdateView(UpdateView):
    """Update platform information"""

    model = Platform
    fields = '__all__'
    template_name = 'apps/updateplatform.html'
    # Use reverse_lazy instead of reverse to prevent circular import error
    success_url = reverse_lazy('platforms')


class PlatformDeleteView(DeleteView):
    """Delete platform information"""

    model = Platform
    fields = '__all__'
    template_name = 'apps/deleteplatform.html'
    # Use reverse_lazy instead of reverse to prevent circular import error
    success_url = reverse_lazy('platforms')


def versiondetails(request, pk):
    """Update version information"""
    assert isinstance(request, HttpRequest)
    logger.info('views.versioninformation: Update version information')

    plugin_version_form = PluginVersionForm(request.POST or None)

    if plugin_version_form.is_valid():
        """Retrieve details submitted from the form."""
        plugin_name = plugin_version_form.cleaned_data.get('plugin_name')
        platform = plugin_version_form.cleaned_data.get('platform')
        activate = plugin_version_form.cleaned_data.get('activate')

        """Get the specified version and repository"""
        version = Versions.objects.get(pk=pk)
        repository = version.repository

        """Update version details"""
        platform = Platform.objects.get(name=platform)
        repository.title = plugin_name
        repository.platform = platform
        repository.save()

        version.author = plugin_version_form.cleaned_data.get('email')
        version.notes = plugin_version_form.cleaned_data.get('notes')
        version.security_version = plugin_version_form.cleaned_data.get('security_version')  # noqa
        version.save()

        """Change version state whether to the active version or deactivate it."""
        change_version_state(pk, activate)

    data = version_data(pk)
    platforms = get_platform_choices()

    return render(
        request, 'apps/versiondetails.html',
        {
            'title': 'Versions Information', 'version_data': data,
            'platforms': platforms, 'form': plugin_version_form
        }
    )
