from django.contrib import admin
from apps.models import (Platform, Repository, Versions)
from apps.tasks import (
    update_repository_database, update_version_database
)
from apps.sub_logger import logger


@admin.action(description='Update repositories and versions')
def update_repositories_and_versions(modeladmin, request, queryset):
    """First update repository"""
    update_repository_database()

    """Then update versions"""
    update_version_database()
    logger.info(
        'admin.update_repositories_and_versions: Updated repositories and versions'
    )


# Register your models here.
class PlatformPage(admin.ModelAdmin):
    list_display = ('name', 'image')


class RepositoryPage(admin.ModelAdmin):
    list_display = ('name', 'title', 'platform')
    actions = [update_repositories_and_versions]


class VersionsPage(admin.ModelAdmin):
    list_display = (
        'repository', 'version_number', 'security_version', 'active'
    )
    actions = [update_repositories_and_versions]


admin.site.register(Platform, PlatformPage)
admin.site.register(Repository, RepositoryPage)
admin.site.register(Versions, VersionsPage)
